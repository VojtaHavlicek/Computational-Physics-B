#include <iostream>
#include <armadillo>
#include "system.hpp"

// Class encapsulating the simulation run itself                
class Simulation                                                
{                                                               
    public:                                                     
        // CONSTRUCTORS                                         
        Simulation();                                           
        Simulation(System* system);                              
        Simulation(System* system, double discard_percentage_);
                                                                
        // METHODS
        void run();
        void run(int iterations);
        // VARIABLES    

        // The following are calculated based on ergodic hypothesis.    
        // after long run, these should equillibriate to        
        // steady values as all microstates are equally likely  
        // (so it does not matter that we started in less       
        // likely state, provided the iteration is long enough) 
        //
        // To avoid unneccessarilly long equillibriation, first 
        // few percent of iteration are discarded however
        //
        // moving averages:
        double average_energy;                                  
        double average_energy_squared;                          
        double average_magnetisation;                           
        double average_magnetisation_squared;                   

        // Percentage of initial measurements on system to
        // discard (before satisfactory equillibriation)
        double discard_percentage;
    
    protected:
        System* system;
        int iterations;

    private:
        // Sums of measured values
        double energy_bulk;
        double energy_squared_bulk;
        double magnetisation_bulk;
        double magnetisation_squared_bulk;

        void updateMovingAverages(System *system, int step);

                                                                
};                                                              
