// com/system.cpp
#include "system.hpp"

// SYSTEM CLASS 
// --------------------------
// Can I specify these constants in the class itself?
const int    default_N              = 10;
const double default_beta           = 1.0;
const double default_magnetic_field = 0.0;

// CONSTRUCTORS
// --------------------------
System::System()
       :System(default_N){};

System::System(int N_)
       :System(N_, false){};

System::System(int N_, bool cold_start)
       :System(N_, cold_start, default_beta){};

System::System(int N_, bool cold_start, double beta_)
       :System(N_, cold_start, beta_,default_magnetic_field) {};

System::System(int N_, bool cold_start, double beta_, double magnetic_field_) 
       :N(N_), beta(beta_), magnetic_field(magnetic_field_)
{
    // Check if N is even and terminate if not.
    // (some calculations rely on N being even)
    if(N_ % 2 != 0)
    {
        std::cerr<< "Please supply even N" << '\n';
        exit(EXIT_FAILURE);
    }

    // If the system does not start at zero temperature
    // generate a completely random vector of spins.
    // Otherwise start with ups everywhere.
    if(! cold_start)                                                            
    {                                                                           
         uvec bools_ =  vec(N*N, fill::randu) > .5; // create a bool of 1 and 0  
         spins = 2*conv_to<vec>::from(bools_) - 1; // converts to +- 1           
    }                                                                           
    else                                                                        
    {                                                                           
        spins = ones(N*N); // if cold initialize all spins up                   
    }                                                                           
                                                                                
    // Calculate the total values of energy, magnetisation 
    // and heat capacity
    recalculateTotalMagnetisation();                                            
    recalculateTotalEnergy();

    //std::cout << total_energy << '\n';
                                                                                
    // Output the system to terminal
    //coutSystem();                                                               

    // Metropolis (~ N steps per spin)
    //for(int j = 0; j < N*N*N; ++j)                                              
    //        update();                                                           
                                                                                
    // Output
    // std::cout << total_energy << '\n';
    // recalculateTotalEnergy();
    // std::cout << total_energy << '\n';
    //coutSystem();                                                               
};

// METHODS
// ------------------------
void System::update()
{
    int delta_energy_;
    int index_;

    deltaEnergy(&delta_energy_, &index_);
    //std::cout << delta_energy_ << '\n';

    if(delta_energy_ < 0){
       flipSpin(index_);
       total_energy += delta_energy_;
    }else
    {
        unsigned seed_ = std::chrono::system_clock::now().time_since_epoch().count();
                         std::mt19937 mersene_twister_(seed_);

        // this may be too greedy
        double rand = double(mersene_twister_())/double(mersene_twister_.max());
        if(rand < exp(-delta_energy_*beta))
        {
           flipSpin(index_);
           total_energy += delta_energy_;
        }
    }
}

// ACCESSORS
// --------------------
double System::getEnergyPerSpin()
{
    std::cerr << "getEnergyPerSpin is not yet implemented!!!" << '\n';
    return 0.0;
}

double System::getMagnetisationPerSpin()
{
    return double(total_magnetisation)/N*N;
}

double System::getSpecificHeatCapacity()
{
    std::cerr << "getSpecificHeatCapacity is not yet implemented!!!" << '\n';
    return 0.0;
}


// UTILITY FUNCTIONS
// --------------------------
void System::coutSystem()
{
    for(int i = 0; i < N*N; ++i)
    {
        cout << int(spins(i) > 0) << ',';
        if(i%N == N-1)
            cout << '\n';
    }

    cout << '\n';
}

// Explicitly calculates the magnetisation in 
// the system.
void System::recalculateTotalMagnetisation() 
{                                            
    total_magnetisation = sum(spins);        
}                                            

// Explicitly calculates the total energy of
// the system
void System::recalculateTotalEnergy()
{
    int this_;
    total_energy = 0;
    // When calculating the neighbours, given that N is even
    // i can just iterate over each second index and count 
    // neighbour interactions to avoid counting twice
    for(int i = 0; i < N*N; ++i)
    {
        this_ = getSpin(i);

        total_energy -= this_*getSpin(getCol(i) + 1, getRow(i));
        total_energy -= this_*getSpin(getCol(i) - 1, getRow(i));
        total_energy -= this_*getSpin(getCol(i), getRow(i) + 1);
        total_energy -= this_*getSpin(getCol(i), getRow(i) - 1);
    }

    total_energy /= 2;

    // Contribution of B field here
    total_energy -= sum(spins)*magnetic_field;
}

// UPDATE STEP FUNCTIONS
// --------------------------------
// Pick a spin at random, calc. difference on flip & return 
// randomly picked index and corresponding energy difference
void System::deltaEnergy(int* ptr_delta_energy_, int* ptr_index_)
{
    int index_ = randomIndex();
    int this_  = getSpin(index_);
                                                                              
    int col_ = getCol(index_);                                                
    int row_ = getRow(index_);                                                

    int siblings_[4] = {getSpin(col_ -1, row_),                               
                        getSpin(col_ +1, row_),                               
                        getSpin(col_, row_ +1),                               
                        getSpin(col_, row_ -1)};                              
                                                                              
    int delta_energy_ = 0;
                                                                              
    for(int i = 0; i < 4; i++)
        delta_energy_ += 2*this_*siblings_[i]; // energy difference in units of J. note the factor of 2 

    // contribution of the magnetic field
    delta_energy_ += this_*magnetic_field*2;
    
    *ptr_delta_energy_ = delta_energy_;
    *ptr_index_ = index_;
}

// Flips the spin at index index_
void System::flipSpin(int index_)
{
    spins(index_) *= -1;
    total_magnetisation += spins(index_)*2;

    // External magnetic field
//    total_energy -= spins(index_)*magnetic_field*2; // Check the sign!
}

// Return a random index. Mersenne twister seeded with 
// timestamp is  used for the choice
int System::randomIndex()
{
    unsigned seed_ = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 mersene_twister_ (seed_); // C++11 mersene twister implementation
    
    return mersene_twister_() % (N*N);
}

// PROTECTED ACCESS METHODS
// ------------------------
int System::getSpin(int col, int row) 
{
    int index_ = ((row+N)%N)*N + (col+N)%N; // indexed value in the array with periodic BC. N is added to prevent negative congruences
    return int(spins(index_));
}

int System::getSpin(int index_) 
{                               
    return int(spins(index_));  
}                               

int System::getCol(int index_)
{
    return ((index_ + N)%N); // N is added to prevent negative congruences
}

int System::getRow(int index_)
{
    return (index_ - getCol(index_)) / N;
}

// Clean memory after these? Is this ok?
// Isn't the array destroyed after execution of the function?
int* System::getNeighbours(int index_)
{
    int col_ = getCol(index_);
    int row_ = getRow(index_);

    int* to_return_ = new int[4];

    to_return_[0] = getSpin(col_ + 1, row_);
    to_return_[1] = getSpin(col_ - 1, row_);
    to_return_[2] = getSpin(col_, row_ +1);
    to_return_[3] = getSpin(col_, row_ -1);

    return to_return_;
}







