// com/system.hpp
// 
// System
// the class encapsulates the Ising NxN lattice
#pragma once

#include <iostream>   // terminal streams
#include <armadillo>  // linear algebra
#include <random>     // mersene twister
#include <chrono>     // seeds

using namespace arma;

class System
{           
    public:
       
        // CONSTRUCTORS
        // ------------------
        System();
        System(int N_);
        System(int N_, bool cold_start); // switch to start from cold temperature
        System(int N_, bool cold_start, double beta_);
        System(int N_, bool cold_start, double beta_, double magnetic_field_);

        // PROPERTIES
        // ------------------
        int N;
        int total_magnetisation; // total magnetisation of the system
        int total_energy; 
        int total_heat_capacity;
        
        double beta; // beta coefficient of temperature
        double magnetic_field; // external B-field

        // ACCESSORS
        // -------------------
        double getEnergyPerSpin();
        double getMagnetisationPerSpin(); 
        double getSpecificHeatCapacity();
        
        // METHODS
        // -------------------
        void update();  // performs a metropolis update on the system
        // compact(); // returns an integer representation of the system? 

    protected:

        // VARIABLES
        // ---------------------
        vec spins;  // Armadillo vector of spins (-1 or 1)
        
        // METHODS
        // ---------------------
         int  getSpin(int col, int row); // returns a value of spin at position i,j (periodic boundary conditions apply)
         int  getSpin(int index);
        void  flipSpin(int index);
         int  getCol(int index); // returns a column from the index in spin vec
         int  getRow(int index);
         int* getNeighbours(int index); // returns spin values of neighbour in int array
         int randomIndex(); // randomly choose a spin in the array

    private:
        void deltaEnergy(int* ptr_delta_energy_, int* ptr_index_);
        void recalculateTotalMagnetisation();
        void recalculateTotalEnergy();
        void coutSystem(); // outputs a bool matrix to console 

};
