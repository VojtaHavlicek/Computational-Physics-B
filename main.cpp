// PROJECT B
// COMPUTATIONAL PHYSICS
//
// VOJTECH HAVLICEK 2013
//
// Simulation of an Ising model using Metropolis algorithm

#include "main.hpp"

// MAIN ENTRY POINT
// ---------------------------
int main(int argv, char** argc)
{
    System system         = System(20, false, 0.0);
    Simulation simulation = Simulation(&system, 0.5);

    simulation.run(50000);
}
