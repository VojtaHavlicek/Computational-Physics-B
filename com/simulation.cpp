#include "simulation.hpp"

const int default_simulation_iterations = 100;
const double default_discard_percentage = 0.1;

// CONSTRUCTORS
Simulation::Simulation(){};

Simulation::Simulation(System* system_)
           :Simulation(system_, default_discard_percentage) {};

Simulation::Simulation(System* system_, double discard_percentage_)
           :discard_percentage(discard_percentage_), system(system_){};

// METHODS
void Simulation::run()
{
    run(default_simulation_iterations);
}

void Simulation::run(int iterations_)
{
    iterations = iterations_; 

    // Clean up first
    energy_bulk = 0;
    energy_squared_bulk = 0;
    magnetisation_bulk = 0;
    magnetisation_squared_bulk = 0;

    for(int i = 0; i < iterations; ++i)
    {
        system->update();

        if(i > discard_percentage*iterations)
            updateMovingAverages(system, i);

    }
}

// updates the moving averages
void Simulation::updateMovingAverages(System *system, int step)
{
    energy_bulk         += system->total_energy;
    energy_squared_bulk += (system->total_energy) * (system->total_energy);
    magnetisation_bulk  += system->total_magnetisation;
    magnetisation_squared_bulk += (system->total_magnetisation)*(system->total_magnetisation);
   
    int steps = (1-discard_percentage)*step;
    average_energy         = energy_bulk/steps;
    average_energy_squared = energy_squared_bulk/steps;
    average_magnetisation  = magnetisation_bulk/steps;
    average_magnetisation_squared = magnetisation_squared_bulk/steps;

    std::cout << average_energy << '\t' << sqrt(average_energy_squared) << '\n'; 
}



